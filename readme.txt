=== GLS for WooCommerce ===
Contributors: GLS
Tags: shipping, multi carrier, save, automate, woocommerce
Requires at least: 4.9
Tested up to: 6.2
Requires PHP: 5.6
Stable tag: trunk 
Author URI: https://gls.pt/
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

GLS for WooCommerce  

== Description == 

= Screenshots =
 
== Installation ==
1. Download and install the plugin. You can also upload the entire “GLS for Woocommerce” folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the ‘Plugins’ menu in WordPress
1. Go to Settings > GLS Settings and insert your keys to get started. 

== Changelog ==