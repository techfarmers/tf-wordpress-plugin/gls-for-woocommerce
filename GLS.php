<?php 
/**
 * Plugin Name: GLS for WooCommerce
 * Description: GLS for WooCommerce
 * Version: 3.1.70
 * Author: GLS
 * Author URI: https://GLS.pt
 * Text Domain: GLS-for-woocommerce
 * @package GLS
 *
 * Woo: 12345:342928dfsfhsf8429842374wdf4234sfd
 * WC requires at least: 3.0.0
 * WC tested up to: 5.5.0
 * 
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

$plugin_url = plugin_dir_url( __FILE__ ); 
# Get rid of the protocol entirely 
$plugin_url = preg_replace('/https?\:/', '', $plugin_url );  

define( 'GLS_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'GLS_PLUGIN_FILE',  __FILE__ );
define( 'GLS_PLUGIN_URL',  $plugin_url );
define( 'GLS_LOGO', GLS_PLUGIN_URL.'assets/images/logo.svg'); 

require_once(GLS_PLUGIN_PATH.'/constants.php'); 
require_once(GLS_PLUGIN_PATH.'includes/class-woo-GLS.php'); 

/** 
 * Marketplace have special needs, vendors have their own keys and the owner of the site might not 
 * have a contract with GLS 
 * @return true if a marketplace we integrate with is installed 
 */ 
function GLS_is_marketplace() {
  global $GLS_wcfm, $GLS_dokan;
  return isset($GLS_wcfm) || isset($GLS_dokan); 
} 
 
$GLS = WooGLS::instance(); 
$GLS->bootstrap(); 