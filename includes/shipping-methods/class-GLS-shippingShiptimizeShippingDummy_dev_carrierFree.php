<?php 
    /** 
     * Declares a shipping method for carrier Dummy dev carrier 
     */ 
    class GLSShippingDummy_dev_carrierFree extends WC_Shipping_Free_Shipping { 
        /**
         * Constructor.
         *
         * @param int $instance_id Instance ID.
         */
        public function __construct( $instance_id = 0){
          global $GLS; 

          $this->instance_id =  absint( $instance_id );
          $this->id = 'shipping_GLS_29_free';
          $this->method_title = 'GLS: Dummy dev carrier Free Shipping'; //name for admin 
          $this->method_description = 'GLS Dummy dev carrier Free Shipping'; // description for admin 
          $this->has_pickup = false;
      
          $this->supports = array(
                'shipping-zones',
                'instance-settings',
                'instance-settings-modal',
          );

          $this->init();

          // Default to returning the string passed by param 
          $translate = function ($str){ return $str;}; 

          // how it's possible that someone declares this method before there's an instance of the plugin is a mistery to solve later 
          // possible they copied the code and did their own  rendition of the thing? 
          if(!$GLS && class_exists('WooGLS')) {
            $GLS = WooGLS::getInstance();
          }

          # If we have an instance of GLS, then use that translate function 
          if($GLS) {
            $translate = function ($str) { global $GLS; return $GLS->translate($str); };
          }

          /*Service Level*/
          
          
          /*Extraoptions */ 
          

          /** checkboxfields **/  
           

          /** exclude classes **/
          

          add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
        } 

        public function validate_excludeclasses_field( $key , $value ) {
          if ( $key === 'excludeclasses' ) {
            return empty($value) ? '' : implode( ',' , $value );
          }
          return $value;
        }
       
        /**
         * Initialize free shipping.
         */
        public function init() { 
          // Load the settings.
          $this->init_form_fields();
          $this->init_settings();

          // Define user set variables.
          $this->title            = $this->get_option( 'title' );
          $this->min_amount       = $this->get_option( 'min_amount', 0 );
          $this->requires         = $this->get_option( 'requires' );
          $this->ignore_discounts = $this->get_option( 'ignore_discounts' );

          // Actions.
          add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
        }

        public function get_admin_options_html()
        {
          if ( $this->instance_id ) {
            $settings_html = $this->generate_settings_html( $this->get_instance_form_fields(), false );
          } else {
            $settings_html = $this->generate_settings_html( $this->get_form_fields(), false );
          }

          $excludeclassesoptions = array();
          if ( is_plugin_active('woocommerce-advanced-shipping/woocommerce-advanced-shipping.php') ) { 
              $shipping  = new \WC_Shipping(); 
              $opt_exclude_classes = explode( ',', $this->get_instance_option('excludeclasses') );               
              $opt_exclude_classes = explode( ',', $this->get_instance_option('excludeclasses') );
          
              foreach ( $shipping->get_shipping_classes() as $shipping_class ) {

                array_push( $excludeclassesoptions, array( 
                  'id' => $shipping_class->term_id,
                  'name' => $shipping_class->name,
                  'selected' => in_array( $shipping_class->term_id , $opt_exclude_classes ) ? 'checked' : ''
                ));
              }
          }
          return '<table class="form-table">' . $settings_html . '</table><script>
          var optionsid = "#woocommerce_' . $this->id . '_extraoptions";
          
          setTimeout( function(){
            console.log("Helloo from '.$this->id.'");

            jQuery(".GLS-extra-option-values").parent().parent().parent().hide();
            GLS_extraoption_values();

            jQuery(optionsid).change(GLS_extraoption_values); 
            setExcludeClasses();

          }, 0);

          function GLS_extraoption_values(){
              var selectedoption = jQuery(optionsid).val(); 
              jQuery(".GLS-extra-option-values").parent().parent().parent().hide();
              jQuery("#woocommerce_' . $this->id . '_extraoptions" + selectedoption).parent().parent().parent().show(); 
          }

          function setExcludeClasses() {
            var excludeoptions =' . json_encode($excludeclassesoptions) . ';
            var select = jQuery("#woocommerce_shipping_GLS_29_free_excludeclasses"); 
            var content = select.parent();
            select.remove();  
            for ( var x=0; x< excludeoptions.length; ++x ) { 
              content.append(\'<span class="GLS-ib GLS-exclude-class"> <input type="checkbox" name="woocommerce_shipping_GLS_29_free_excludeclasses[]" value="\' + excludeoptions[x].id + \'" \' + excludeoptions[x].selected + \' /> \' + excludeoptions[x].name + \'</span>\');
            }
          }
          </script>';
        }
 
    }