<?php
    use Wbs\ShippingMethod; 

    /** 
     * Declares a shipping method for carrier DHL Parcel (2C) 
     */ 
    class GLSShippingDHL_Parcel_2C_Weight extends ShippingMethod { 
        /**
         * Constructor.
         *
         * @param int $instance_id Instance ID.
         */
        public function __construct( $instance_id = 0){ 

          $this->instance_id        = absint( $instance_id );
          $this->id                 = 'shipping_GLS_25_weight';
          $this->plugin_id = 'wbs';
          $this->title       =  'DHL Parcel (2C) for Weight Based Shipping'; //name for end user 
          $this->method_title =  'GLS: DHL Parcel (2C) for Weight Based Shipping'; //name for admin 
          $this->method_description = 'GLS DHL Parcel (2C)'; // description for admin 
          $this->has_pickup = true;
          
          $this->supports = array( 
            'shipping-zones',
            'instance-settings',
          );

          $this->init_settings();
        }         

        /** 
         * @override 
         * We don't want our methods confused with the global settings which has instance_id= '' 
         * Only called in wp-admin/
         */ 
        public function get_option_key()
        {
            if(!$this->instance_id ){
              return ''; 
            }

            $option_key =  join('_', array_filter(array(
                $this->plugin_id,
                $this->instance_id,
                'config',
            )));

            return $option_key;
        }

        public function get_admin_options_html()
        {
          global $GLS; 
          
          // Default to returning the string passed by param 
          $translate = function ($str){ return $str;}; 

          // how it's possible that someone declares this method before there's an instance of the plugin is a mistery to solve later 
          // possible they copied the code and did their own  rendition of the thing? 
          if(!$GLS && class_exists('WooGLS')) {
            $GLS = WooGLS::getInstance();
          }

          # If we have an instance of GLS, then use that translate function 
          if($GLS) {
            $translate = function ($str) { global $GLS; return $GLS->translate($str); };
          }

            $GLS_options = json_encode(get_option('wbs_'.$this->instance_id.'_GLS'));
            $pickup_behaviour_label = $GLS->translate('pickupbehaviour');
            $pickup0 = $translate('pickuppointbehavior0');
            $pickup1 = $translate('pickuppointbehavior1');
            $pickup2 = $translate('pickuppointbehavior2');
            $extraoptions = $translate('extraoptions');
            $servicelevel = $translate('service_level');

            ob_start(); 
                echo "<script>
                var GLS_carrier={\"Id\":\"25\",\"Name\":\"DHL Parcel (2C)\",\"HasPickup\":true,\"MapFields\":[{\"Name\":\"Packstation ID\",\"Id\":\"packstationid\"}],\"OptionList\":[{\"Id\":\"40\",\"ClassId\":\"40\",\"Name\":\"DHL product\",\"Type\":1},{\"Name\":\"Evening delivery\",\"Type\":0,\"IsPickup\":0,\"Id\":\"42\",\"ClassId\":\"42\"},{\"Id\":\"44\",\"ClassId\":\"44\",\"IsPickup\":1,\"Type\":0,\"Name\":\"Parcelshop delivery\"},{\"ClassId\":\"46\",\"Id\":\"46\",\"Name\":\"Brievenbus pakje\",\"IsPickup\":0,\"Type\":0},{\"Type\":0,\"IsPickup\":0,\"Name\":\"Do not deliver at neighbour\",\"Id\":\"49\",\"ClassId\":\"49\"},{\"Name\":\"Delivery with signature\",\"Type\":0,\"IsPickup\":0,\"ClassId\":\"5\",\"Id\":\"5\"},{\"IsPickup\":0,\"Type\":0,\"Name\":\"Send insured\",\"ClassId\":\"57\",\"Id\":\"57\"},{\"Type\":0,\"IsPickup\":0,\"Name\":\"Return shipment\",\"Id\":\"60\",\"ClassId\":\"60\"},{\"ClassId\":\"68\",\"Id\":\"68\",\"Type\":0,\"IsPickup\":0,\"Name\":\"Leeftijd controle\"}]};var GLS_extraoptions=[\"2\",\"47\",\"46\",\"49\",\"42\",\"62\",\"55\",\"73\",\"80\"];var GLS_checkboxes={\"13\":\"sendinsured\",\"31\":\"sendinsured\",\"32\":\"activatepickup\",\"57\":\"sendinsured\",\"70\":\"fragile\",\"56\":\"activatepickup\",\"59\":\"activatepickup\"};GLS_labels = {
          'pickupbehaviour' : \"$pickup_behaviour_label\",
          'pickup0' : \"$pickup0\",
          'pickup1' : \"$pickup1\",
          'pickup2' : \"$pickup2\",
          'extraoptions': \"$extraoptions\",
          'servicelevel':\"$servicelevel\",
    };

                //previous options 
                var GLS_options = $GLS_options;
                </script>";
                /** @noinspection PhpIncludeInspection */
                include(Wbs\Plugin::instance()->meta->paths->tplFile);
            return ob_get_clean();
        }
    }