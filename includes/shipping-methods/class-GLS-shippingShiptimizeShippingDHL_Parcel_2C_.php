<?php 
    /** 
     * Declares a shipping method for carrier DHL Parcel (2C) 
     */ 
    class GLSShippingDHL_Parcel_2C_ extends WC_Shipping_Flat_Rate { 
        /**
         * Constructor.
         *
         * @param int $instance_id Instance ID.
         */
        public function __construct( $instance_id = 0){
          global $GLS; 

          $this->instance_id = absint( $instance_id );
          $this->id = 'shipping_GLS_25';
          $this->title = 'DHL Parcel (2C) Flat Rate'; //name for end user 
          $this->method_title = 'GLS: DHL Parcel (2C) Flat Rate'; //name for admin 
          $this->method_description = 'GLS DHL Parcel (2C)'; // description for admin 
          $this->has_pickup = true;
      
          $this->supports = array(
                'shipping-zones',
                'instance-settings',
                'instance-settings-modal',
          );

          // Default to returning the string passed by param 
          $translate = function ($str){ return $str;}; 

          // how it's possible that someone declares this method before there's an instance of the plugin is a mistery to solve later 
          // possible they copied the code and did their own  rendition of the thing? 
          if(!$GLS && class_exists('WooGLS')) {
            $GLS = WooGLS::getInstance();
          }

          # If we have an instance of GLS, then use that translate function 
          if($GLS) {
            $translate = function ($str) { global $GLS; return $GLS->translate($str); };
          }

          $this->init();
          $this->options = get_option($this->id . '_' . $this->instance_id . '_settings');
          
          /*Service Level*/
          $this->instance_form_fields['service_level'] = array(
                'title'             => $translate('service_level'),
                'type'              => 'select',
                'class'       => 'GLS-service-level',
                'default'           => 0, 'options' => array( "" => "-" )  );
          $this->service_level = $this->get_option( 'service_level' , '');
          
          /*Extraoptions */ 
          

$this->instance_form_fields['pickupbehaviour'] = array(
                'title'             => $translate('pickupbehaviour'),
                'type'              => 'select',
                'class'       => 'GLS-pickupbehaviour',
                'default'           => 0, 'options' => array('0'=> "Optional", '1' => "Mandatory", '2' =>"Disabled")  );
          $this->pickupbehaviour = $this->get_option( 'pickupbehaviour' , '');

$this->instance_form_fields['extraoptions'] = array(
                'title'             => $translate('extraoptions'),
                'type'              => 'select',
                'class'       => 'GLS-extra-options',
                'default'           => 0, 'options' => array('0'=>'-','42'=>'Evening delivery','46'=>'Brievenbus pakje','49'=>'Do not deliver at neighbour')  );
          $this->extraoptions = $this->get_option( 'extraoptions' , '');

          /** checkboxfields **/  
           

          /** Exclude classes **/ 
          

         
          add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) ); 
 
        } 

        public function validate_excludeclasses_field( $key , $value ) {
          if ( $key === 'excludeclasses' ) {
            return empty($value) ? '' : implode( ',' , $value );
          }
          return $value;
        }

        public function get_admin_options_html()
        {
          if ( $this->instance_id ) {
            $settings_html = $this->generate_settings_html( $this->get_instance_form_fields(), false );
          } else {
            $settings_html = $this->generate_settings_html( $this->get_form_fields(), false );
          }

          
          $excludeclassesoptions = array();
          if ( is_plugin_active('woocommerce-advanced-shipping/woocommerce-advanced-shipping.php') ) { 
              $shipping  = new \WC_Shipping(); 
              $opt_exclude_classes = explode( ',', $this->get_instance_option('excludeclasses') );
              
              foreach ( $shipping->get_shipping_classes() as $shipping_class ) {

                array_push( $excludeclassesoptions, array( 
                  'id' => $shipping_class->term_id,
                  'name' => $shipping_class->name,
                  'selected' => in_array( $shipping_class->term_id , $opt_exclude_classes ) ? 'checked' : ''
                ));
              }
          }
          return '<table class="form-table">' . $settings_html . '</table><script>
          var optionsid = "#woocommerce_' . $this->id . '_extraoptions";
          
          setTimeout( function(){
            console.log("Helloo from '.$this->id.'");

            jQuery(".GLS-extra-option-values").parent().parent().parent().hide();
            GLS_extraoption_values();

            jQuery(optionsid).change(GLS_extraoption_values); 

            setExcludeClasses();
          }, 0);

          function GLS_extraoption_values(){
              var selectedoption = jQuery(optionsid).val(); 
              jQuery(".GLS-extra-option-values").parent().parent().parent().hide();
              jQuery("#woocommerce_' . $this->id . '_extraoptions" + selectedoption).parent().parent().parent().show(); 
          }

          function setExcludeClasses() {
            var excludeoptions =' . json_encode($excludeclassesoptions) . ';
            var select = jQuery("#woocommerce_shipping_GLS_25_excludeclasses"); 
            var content = select.parent();
            select.remove();  
            for ( var x=0; x< excludeoptions.length; ++x ) { 
              content.append(\'<span class="GLS-ib GLS-exclude-class"> <input type="checkbox" name="woocommerce_shipping_GLS_25_excludeclasses[]" value="\' + excludeoptions[x].id + \'" \' + excludeoptions[x].selected + \' /> \' + excludeoptions[x].name + \'</span>\');
            }


          }
          </script>';
        }
    }