<?php
    use Wbs\ShippingMethod; 

    /** 
     * Declares a shipping method for carrier CTT 
     */ 
    class GLSShippingCTTWeight extends ShippingMethod { 
        /**
         * Constructor.
         *
         * @param int $instance_id Instance ID.
         */
        public function __construct( $instance_id = 0){ 

          $this->instance_id        = absint( $instance_id );
          $this->id                 = 'shipping_GLS_30_weight';
          $this->plugin_id = 'wbs';
          $this->title       =  'CTT for Weight Based Shipping'; //name for end user 
          $this->method_title =  'GLS: CTT for Weight Based Shipping'; //name for admin 
          $this->method_description = 'GLS CTT'; // description for admin 
          $this->has_pickup = true;
          
          $this->supports = array( 
            'shipping-zones',
            'instance-settings',
          );

          $this->init_settings();
        }         

        /** 
         * @override 
         * We don't want our methods confused with the global settings which has instance_id= '' 
         * Only called in wp-admin/
         */ 
        public function get_option_key()
        {
            if(!$this->instance_id ){
              return ''; 
            }

            $option_key =  join('_', array_filter(array(
                $this->plugin_id,
                $this->instance_id,
                'config',
            )));

            return $option_key;
        }

        public function get_admin_options_html()
        {
          global $GLS; 
          
          // Default to returning the string passed by param 
          $translate = function ($str){ return $str;}; 

          // how it's possible that someone declares this method before there's an instance of the plugin is a mistery to solve later 
          // possible they copied the code and did their own  rendition of the thing? 
          if(!$GLS && class_exists('WooGLS')) {
            $GLS = WooGLS::getInstance();
          }

          # If we have an instance of GLS, then use that translate function 
          if($GLS) {
            $translate = function ($str) { global $GLS; return $GLS->translate($str); };
          }

            $GLS_options = json_encode(get_option('wbs_'.$this->instance_id.'_GLS'));
            $pickup_behaviour_label = $GLS->translate('pickupbehaviour');
            $pickup0 = $translate('pickuppointbehavior0');
            $pickup1 = $translate('pickuppointbehavior1');
            $pickup2 = $translate('pickuppointbehavior2');
            $extraoptions = $translate('extraoptions');
            $servicelevel = $translate('service_level');

            ob_start(); 
                echo "<script>
                var GLS_carrier={\"OptionList\":[{\"Id\":\"11\",\"ClassId\":\"11\",\"Name\":\"Pick &amp; return\",\"IsPickup\":0,\"Type\":0},{\"Name\":\"Pick &amp; ship\",\"Type\":0,\"IsPickup\":0,\"Id\":\"12\",\"ClassId\":\"12\"},{\"ClassId\":\"31\",\"Id\":\"31\",\"Type\":2,\"IsPickup\":0,\"OptionFields\":[{\"Name\":\"Send insured\",\"Id\":\"1\"}],\"Name\":\"Send insured (checkbox)\"},{\"Type\":2,\"IsPickup\":0,\"OptionFields\":[{\"Name\":\"Colli count\",\"Id\":\"1\"}],\"Name\":\"Colli count\",\"ClassId\":\"35\",\"Id\":\"35\"},{\"Id\":\"54\",\"ClassId\":\"54\",\"Type\":1,\"OptionValues\":[{\"Name\":\"13h\",\"IsPickup\":0,\"Id\":\"10\"},{\"Name\":\"19h\",\"IsPickup\":0,\"Id\":\"11\"},{\"Name\":\"22h\",\"IsPickup\":0,\"Id\":\"12\"},{\"IsPickup\":0,\"Id\":\"13\",\"Name\":\"Easy Return - Devolu\\u00e7\\u00f5es F\\u00e1ceis\"},{\"Name\":\"48h\",\"Id\":\"2\",\"IsPickup\":0},{\"Name\":\"19h M\\u00faltiplo\",\"Id\":\"3\",\"IsPickup\":0},{\"Name\":\"13h M\\u00faltiplo\",\"IsPickup\":0,\"Id\":\"4\"},{\"IsPickup\":0,\"Id\":\"5\",\"Name\":\"Internacional\"},{\"IsPickup\":0,\"Id\":\"7\",\"Name\":\"Em 2 dias\"},{\"Name\":\"Internacional Economy\",\"IsPickup\":0,\"Id\":\"8\"},{\"IsPickup\":0,\"Id\":\"9\",\"Name\":\"10h\"}],\"Name\":\"Service level\"},{\"Name\":\"Fragile\",\"IsPickup\":0,\"Type\":2,\"OptionFields\":[{\"Name\":\"Fragile\",\"Id\":\"1\"}],\"ClassId\":\"70\",\"Id\":\"70\"},{\"Type\":0,\"IsPickup\":1,\"Name\":\"Delivery Point\",\"ClassId\":\"71\",\"Id\":\"71\"},{\"Name\":\"Delivery Attempts\",\"IsPickup\":0,\"Type\":0,\"OptionFields\":[{\"Id\":\"1\",\"Name\":\"Delivery Attempts\",\"OptionValues\":[{\"Id\":\"2\",\"Name\":\"2\"},{\"Id\":\"3\",\"Name\":\"3\"}]}],\"ClassId\":\"73\",\"Id\":\"73\"},{\"Id\":\"80\",\"ClassId\":\"80\",\"Name\":\"Add return label\",\"Type\":0,\"IsPickup\":0}],\"HasPickup\":true,\"Name\":\"CTT\",\"Id\":\"30\"};var GLS_extraoptions=[\"2\",\"47\",\"46\",\"49\",\"42\",\"62\",\"55\",\"73\",\"80\"];var GLS_checkboxes={\"13\":\"sendinsured\",\"31\":\"sendinsured\",\"32\":\"activatepickup\",\"57\":\"sendinsured\",\"70\":\"fragile\",\"56\":\"activatepickup\",\"59\":\"activatepickup\"};GLS_labels = {
          'pickupbehaviour' : \"$pickup_behaviour_label\",
          'pickup0' : \"$pickup0\",
          'pickup1' : \"$pickup1\",
          'pickup2' : \"$pickup2\",
          'extraoptions': \"$extraoptions\",
          'servicelevel':\"$servicelevel\",
    };

                //previous options 
                var GLS_options = $GLS_options;
                </script>";
                /** @noinspection PhpIncludeInspection */
                include(Wbs\Plugin::instance()->meta->paths->tplFile);
            return ob_get_clean();
        }
    }