<?php 

require_once(GLS_PLUGIN_PATH.'/includes/class-woo-GLS-order.php');

/**
 * this class handles the ui actions for order management 
 *  
 *
 * @package GLS.admin
 * @since   1.0.0
 */ 
class GLSOrderUI {
     
  public function __construct() {
    $this->add_filters(); 
    $this->add_actions();
 
  }

  /**
  * adds columns to the order view 
  * adds buttons to export to GLS 
  * https://codex.wordpress.org/Plugin_API/Filter_Reference/manage_edit-post_type_columns 
  * columns available since wp 3.1 
  * 
  * @return void 
  */ 
  public function add_filters(){
    add_filter( 'manage_edit-shop_order_columns', array($this,'order_columns') ); 
    add_filter( 'posts_join', array($this, 'query_filter_order_status_join') );
    add_filter( 'posts_where', array($this, 'query_filter_order_status_where') );
    add_filter( 'bulk_actions-edit-shop_order', array( $this, 'register_bulk_export' ) );
    add_filter( 'handle_bulk_actions-edit-shop_order', array( $this, 'handle_bulk' ), 10, 3 ); 
  }

  /** 
   * WP actions for the order ui 
   * @return void 
   */  
  public function add_actions(){
    add_action( 'manage_shop_order_posts_custom_column', array($this,'order_column_values'));
    add_action( 'restrict_manage_posts' , array($this, 'action_filter_order_status') );
    add_action( 'admin_head', array( $this, 'custom_js_to_head') );
    add_action( 'admin_init', array($this, 'admin_init')); 
    add_action( 'wp_ajax_GLS_print_label', array($this, 'ajax_print_label') ); 
    add_action( 'wp_ajax_GLS_label_status', array($this, 'ajax_monitor_label_status') ); 
  }

  /**
   *  
   */
  public function ajax_print_label() {
    $orderid = $_POST['orderid']; 

    if (!$orderid) {
      die(json_encode(array('Error' => 'No order id, cannot print label'))); 
    }

    die(json_encode(WooGLSOrder::print_label(array($orderid))));
  }

  public function ajax_monitor_label_status() {
    global $GLS; 
    $callbackurl = $_POST['callbackUrl']; 

    WooGLS::log("requesting label status from $callbackurl"); 
    $response = WooGLS::get_api()->monitor_label_status($callbackurl); 
    WooGLS::log("label_response " . var_export($response, true));
    if (isset($response->response->Finished) && $response->response->Finished == 100) {
      if (isset($response->response->ClientReferenceCodeList)) {
        foreach($response->response->ClientReferenceCodeList as $labelresult) {
          $order = new WooGLSOrder($labelresult->ShopItemId);
          $status = GLSOrder::$LABEL_STATUS_NOT_REQUESTED; 
          $msg =  '';
          $labelurl = ''; 

          if ($labelresult->Error->Id == 0 ) {
            $status = GLSOrder::$LABEL_STATUS_PRINTED; 
            $labelurl = $response->response->LabelFile; // all labels in this batch share the same url  
            $msg = $GLS->translate('labelprinted');
            $order->set_tracking_id($labelresult->TrackingId, $labelurl); 
          } 
          else {
            $status = GLSOrder::$LABEL_STATUS_ERROR; 
            $msg = $labelresult->Error->Info;
          }

          $order_meta = $order->get_order_meta(); 
          $msg = $order_meta->message . $msg;  
          $labelresult->message = $msg; 
          $order->set_label_meta($labelresult->ShopItemId,$status,$labelurl,$msg);
        }    
      }  
    }

    die(json_encode($response)); 
  }
 

  /**
   * if GLS_action is set to export-all then export all not already set as successfully exported 
   *
   * @return mixed an object describing the success of the export 
   */
  public function admin_init() {
    $GLS_export = filter_input(INPUT_GET, 'GLS_action'); 
    
    switch ($GLS_export) {
      case  'export-all' : 
        return WooGLSOrder::export_all(); 
      case 'export-this':
        $summary =  WooGLSOrder::export(array($_GET['post']));
        wp_redirect(WooGLSOrder::get_redirect_from_summary($summary));
        die();
      default: 
        break;
    } 
  }



  /**
   * Adds filters to select specific order status 
   *
   */ 
  public function action_filter_order_status() {
    global $GLS; 

    $type = isset( $_GET['post_type'] ) ? $_GET['post_type'] : 'post' ; 
    $status = isset( $_GET['GLS_status'] ) ? $_GET['GLS_status'] : ''; 
    if( $type == 'shop_order' ) {
    ?>
      <span class='shitpimize-status-filter'> 
        <select name='GLS_status'>
          <option value=''> <?php echo $GLS->translate('All') ?> </option>
          <?php foreach(WooGLSOrder::$status_text as $id => $status_text) { ?>
            <option value="<?php echo $id?>" <?php echo $status == $id ? 'selected' :'' ?>> <?php echo $status_text ?> </option>
          <?php } ?> 
        </select>
      </span>
    <?php       
    }  
  }

  public function notice_area () {
    echo "<div id=\"GLS_notices\"></div>"; 
  }

  /**
   * filter the orders by status 
   * 
   * @return string - the join to include GLS meta 
   */
  public function query_filter_order_status_join( $join ) {
    global $pagenow, $wpdb; 

    $type = isset( $_GET['post_type'] )?  $_GET['post_type'] : 'post'; 
    $status = isset( $_GET['GLS_status'] ) ? $_GET['GLS_status'] : ''; 
    if( $type == 'shop_order' && $status ){
      $join .= " LEFT JOIN {$wpdb->prefix}GLS on {$wpdb->prefix}GLS.id = {$wpdb->prefix}posts.ID ";
    }

    return $join; 
  }

  /** 
   * filter the posts if status is set 
   */ 
  public function query_filter_order_status_where( $where ) {
    global $wpdb; 

    $type = isset( $_GET['post_type'] )?  $_GET['post_type'] : 'post'; 
    $status = isset( $_GET['GLS_status'] ) ? $_GET['GLS_status'] : ''; 
    if( $type == 'shop_order' && $status ){
      $where .= $status != 1 ? ' AND ' .$wpdb->prefix.'GLS.status='.$status : ' AND ' .$wpdb->prefix.'GLS.status is null' ;
    }
    return $where; 
  }

  /** 
   * add the column headers 
   */ 
  public function order_columns( $columns ) {
    global $GLS; 

    $columns['GLS_status'] = GLS_BRAND . ' ' . $GLS->translate('GLScolumntitle');  
    return $columns; 
  }

  /**
   * add the column values  
   * https://codex.wordpress.org/Plugin_API/Action_Reference/manage_$post_type_posts_custom_column
   *
   * @param string column 
   * @param int post_id 
   */
  public function order_column_values( $column ) {
    global $post; 

    switch ($column) {
      case 'GLS_status': 
        $order_meta = WooGLSOrder::get_shipping_meta( $post->ID ); 
        echo $this->get_status_icon($order_meta ? $order_meta : (object)array('id' => $post->ID, 'message' => '', 'status' => ''));
        break;
    }
  }

  /**
   * returns a representation of this status 
   * @param mixed $order_meta- order metadata 
   *
   * @returnstring containing the html representation of this order's status
   */
  public function get_status_icon($order_meta){ 
    global $GLS; 

    $class = '';
    $msgclass = ''; 

    $message = $GLS->translate('Not Exported');

    if( $order_meta ) {
      
      $message = $order_meta->message;

      if( strlen($message > 100) ){
        $msgclass .= ' GLS-message-large';
      }

      switch ($order_meta->status) { 
        case GLSOrder::$STATUS_EXPORTED_SUCCESSFULLY:
          $class = 'GLS-icon-success';
          break; 
        case GLSOrder::$STATUS_EXPORT_ERRORS:  
          $class = 'GLS-icon-error'; 
          break;
        case GLSOrder::$STATUS_TEST_SUCCESSFUL: 
          $class = 'GLS-icon-test-successful';
          break; 
        case GLSOrder::$LABEL_STATUS_PRINTED: 
          $class = 'GLS-icon-print-printed';
          break; 

        case GLSOrder::$LABEL_STATUS_ERROR: 
          $class = 'GLS-icon-print-error'; 
          break; 

        case GLSOrder::$LABEL_STATUS_NOT_REQUESTED: 
          $class = 'GLS-icon-print-notprinted'; 
          break;

        default: //Not exported or no status 
          $class .= ' GLS-icon-not-exported';  
          $message  .= $message ? '' : $GLS->translate('Not Exported');    
      } 

    } else {
      $class .= ' GLS-icon-not-exported';
    }

    if( isset($order_meta->pickup_label ) &&  $order_meta->pickup_label ){
      $message .= '<br/>'.$GLS->translate('Pickup Point') . ' -  ' . $order_meta->pickup_label; 
    } 
  
    // Also append the option to create a label 
    if (!GLS_is_marketplace()) { 
      $labelagree = get_option('GLS_labelagree');
      $btnlabellabel = $labelagree ? $GLS->translate('printlabel') : $GLS->translate('labellocked'); 
      $labelclick = $labelagree ? "GLS.printlabel(event,$order_meta->id);" : 'event.stopPropagation()';
      $labelbtn = "<span class=\"GLS-status GLS-tooltip-wrapper\">
          <span class=\"GLS-tooltip-reference\">
            <span class=\"button GLS-icon GLS-btn-label-print\"  title=\"label\" onclick='$labelclick'></span>
          </span>

          <span class=\"GLS-tooltip-message\">
            <span class=\"GLS-tooltip-message__arrow\"></span>
            <span class=\"GLS-tooltip__inner\">$btnlabellabel</span>
          </span>
        </span>";
    }
    else {
      $labelbtn = '';
    }

    return '<span class="GLS-status GLS-tooltip-wrapper">
      <span class="GLS-tooltip-reference ' . $msgclass . '">
        <span id="GLS-label' . $order_meta->id . '" class="GLS-icon ' . $class . '"></span>
      </span>
      <span class="GLS-tooltip-message">
        <span class="GLS-tooltip-message__arrow"></span>
        <span class="GLS-tooltip__inner" id="GLS-tooltip' . $order_meta->id . '">'.$message.'</span>
      </span>
    </span>' . $labelbtn; 
  }


  /** 
   * Adds an action to the bulk action menu 
   *
   * @param array $bulk_actions - associative array of bulk actions 
   * @return void 
   */ 
  public function register_bulk_export($bulk_actions) { 
    global $GLS; 

    $bulk_actions['GLS_export'] = $GLS->translate( 'Export to' ) . ' GLS';
    $bulk_actions['GLS_printlabel'] =  'GLS: ' . $GLS->translate('printlabel'); 
    return $bulk_actions; 
  }

  public function handle_bulk ($redirect_to, $doaction, $post_ids) { 

    if( $doaction == 'GLS_export' ){
      return $this->bulk_export( $post_ids ); 
    } 
    else if( $doaction == 'GLS_printlabel') {
      return $this->bulk_print_label ( $post_ids ); 
    }

    return $redirect_to;
  }

  /**
   * Printing Labels is an assynchronous process  
   * At this time it's only possible to print 1 label at a time 
   */
  public function bulk_print_label($post_ids) {
    global $GLS; 

    $summary = WooGLSOrder::print_label($post_ids);  

    # Too Many labels 
    if ( isset( $summary->response->ErrorList ) && count( $summary->response->ErrorList ) > 0 && $summary->response->ErrorList[0]->Id ==  6 ) {
      return admin_url( "edit.php?post_type=shop_order&paged=" . filter_input( INPUT_GET, 'paged' ) . '&Error=' . $GLS->translate( 'multiorderlabelwarn' ) ); 
    }

    # Other high level errors 
    if ( isset($summary->response->ErrorList) && count($summary->response->ErrorList) > 0 && $summary->response->ErrorList[0]->Id > 0 ) {
      return admin_url("edit.php?post_type=shop_order&paged=" . filter_input(INPUT_GET, 'paged') . '&Error=' . $summary->ErrorList[0]->Info); 
    }

    if ( $summary->httpCode == 500 ){
       return admin_url("edit.php?post_type=shop_order&paged=" . filter_input(INPUT_GET, 'paged') . '&Error=Fatal API error. Contact support with the order ids you just tried to print'); 
    }

    $urlString = '';  
    
    WooGLS::log ( "bulk_export summary " . var_export($summary, true) );
    if (isset($summary->orderresponse) && !isset($summary->response)) {
      $summary->response = $summary->orderresponse;
    } 

    if (isset($summary->response)) {
      if(isset($summary->response->CallbackURL)) { 
        $urlString .= '&CallbackURL=' . $summary->response->CallbackURL;
      }
      else if (isset($summary->Error) && $summary->Error->Id > 0) {
        $urlString .='&Error=' . $summary->Error->Info;
      }
      elseif (isset($summary->ErrorList))  {
        $errors = '';
        foreach($summary->ErrorList as  $error) {
          WooGLS::log ( "bulk_export summary "); 
          $errors .= $error->Info . ' ';
        }
        $urlString .= '&Error=' . $errors; 
      }  
    } 


    return admin_url("edit.php?post_type=shop_order&paged=" . filter_input(INPUT_GET, 'paged') . $urlString);
  }

  /** 
   * Do the bulk export  
   */
  public function bulk_export( $post_ids ) {   
    $summary = WooGLSOrder::export($post_ids);  
    $urlString = ''; 

    
    WooGLS::log( "bulk_export summary " . var_export($summary, true) );
    
 
    foreach((array)$summary as $key => $value) { 
      if(!is_array($value)){
        $urlString .= '&GLS_' . $key . '=' . $value;  
      } 
    }

    return admin_url("edit.php?post_type=shop_order&paged=" . filter_input(INPUT_GET, 'paged') . $urlString);
  }  


  public function custom_js_to_head() {
    global $GLS;  

    if(isset($_GET['GLS_nOrders'])){
        $summary = (object)array(
          'n_success' => filter_input(INPUT_GET, 'GLS_n_success'),
          'n_errors' => filter_input(INPUT_GET, 'GLS_n_errors'),
          'login_url' => filter_input(INPUT_GET, 'GLS_login_url'),
          'nOrders' => filter_input(INPUT_GET, 'GLS_nOrders'),
          'nInvalid' => filter_input(INPUT_GET, 'GLS_nInvalid'),
          'message' => filter_input(INPUT_GET, 'GLS_message') 
        );
        $export_message = WooGLSOrder::get_export_summary($summary);
    }

    $class = 'page-title-action GLS-export-btn ' . GLS_BRAND; 

    $is_single_order  = isset($_GET['post']);
    $export_url =  $is_single_order ? admin_url('edit.php?post_type=shop_order&GLS_action=export-this&post='.$_GET['post']) : admin_url('edit.php?post_type=shop_order&GLS_action=export-all'); 
    $export_link_text = ($is_single_order ?  $GLS->translate('Export to') : $GLS->translate("Export Preset Orders to") ) . ' ' .  GLS_BRAND;

  ?>
    <script>
    var GLS_label_request = '<?php echo $GLS->translate('requestinglabel') ?>'; 
    var GLS_label_click = '<?php echo $GLS->translate('labelclick'); ?>'; 
    var GLS_label_label = '<?php echo $GLS->translate('label');?>';

    jQuery(function(){
      let eHeader = jQuery("body.post-type-shop_order .wrap h1");
      eHeader.append('<a href="<?php echo $export_url ?>" class="<?php echo $class ?>"><?php echo $export_link_text ?></a>');

      <?php if($is_single_order) { ?>
        eHeader.append('<a href="#!" onclick="GLS.printlabel(event,<?php echo $_GET['post'] ?>)" class="page-title-action GLS-btn-print-label"><?php echo $GLS->translate('printlabel') ?></a>');        
      <?php } ?>

      <?php if( isset($summary )) { ?>
        var eGLS = jQuery("<?php echo str_replace(array("\n","\""), array("","\\\""), $export_message) ?>");
        jQuery(".wp-header-end").before(eGLS);
        GLS.exportSuccess("<?php echo $summary->login_url?>");
        console.log(eGLS.get(0));
      <?php } ?>


    });
    </script>
  <?php
  }

}

new GLSOrderUI();