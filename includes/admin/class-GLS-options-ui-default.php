<?php 
include_once (GLS_PLUGIN_PATH.'/includes/admin/class-GLS-options-ui.php');

class GLSOptionsUIDefault extends GLSOptionsUI {

	public function getTextInput($label,$name,$value) 
	{
		return  '<div class="gls-input-title">' . $label . '</div>'
		. '<input type="text" class="gls-input" name="' . $name . '" value="' . $value  . '"/>'; 
	}

	public function print_gls_options()
	{ 
		print '<div id="gls-settings">';
		$this->print_banner(); 
		$this->print_tabs();
		$this->print_settings_tab();
		$this->print_help_tab();
		print '</div>';
	}


	public function print_banner()
	{
		global $GLS; 

		echo '<div id="gls-banner">'; 
		echo '<div class="gls-banner--inner">'; 
		echo '<img src="' . GLS_PLUGIN_URL . '/assets/images/logo.svg"/>'; 
		echo '<span>' . $GLS->translate('new_account') . ' <a href='  . GLS_CREATE_ACCOUNT . ' target="_blank">' . $GLS->translate("Click Here")  . '</a></span>';
		echo '</div>'; 
		echo '</div>';  
	}

	public function print_tabs()
	{
		global $GLS; 

		echo '<div class="gls-tabs">'; 

		echo '<span class="gls-tabs--tab nav-tab nav-tab-active" onclick="GLS.platform.selectTab(0)">';
		echo $GLS->translate('settings');
		echo '</span>'; 


		echo '<span class="gls-tabs--tab nav-tab" onclick="GLS.platform.selectTab(1)">';
		echo $GLS->translate('help');
		echo '</span>'; 

		echo '</div>';
	}

	public function print_settings_tab()
	{
		print '<div class="gls-tab tab active">'; 
		$this->print_credentials();
		$this->print_export_options();
		$this->print_pickup_points_options();
		$this->print_label_options();
		$this->print_hide_not_free(); 
		$this->print_export_virtual();
		$this->print_flexible_checkout_fields();
		submit_button();
		print '</div>';
	}

	public function print_accordion($title, $content, $open)
	{

		print '<div class="gls-accordion ' . ($open ? 'open' : '') . '">'; 
		print '<div class="gls-accordion--title" onclick="GLS.platform.accordion(this)">' . $title . '<span class="gls-accordion--icon-close"></span></div>';
		print '<div class="gls-accordion--inner">' . $content . '</div>'; 
		print '</div>'; 
	}

	public function print_credentials()
	{
		global $GLS; 

		$content = $this->getTextInput($GLS->translate('Public Key'),'GLS_public_key',$this->obfuscate($this->public_key));
		$content.= $this->getTextInput($GLS->translate('Private Key'),'GLS_private_key',$this->obfuscate($this->private_key)); 


		if (strlen($this->token)) {
			$content .= '<div class="gls-input-title">Token</div>'; 
			$content .= '<div class="gls-flex-space-between">' . $this->token . '<span><label class="gls-label">' . $GLS->translate('expires at') . ':</label> ' . $this->token_expires . '</span></div>';  
			$content .= '<p><small>' . $GLS->translate('A new token will be automatically requested when this one expires') . '</small></p>'; 

			$content .= '<div class="gls-input-title">' . $GLS->translate('Carriers Available In your contract') . '</div>'; 

			if ($this->carriers) { 
				$i = 0; 
				foreach( $this->carriers as $carrier ) {
					$content .= ( $i++ ? ", ": "" ) . $carrier->Name . ( $carrier->HasPickup ? ' - ' . $GLS->translate('Has Pickup') : '');
				}
 
 	    		$content .= '<p><small>' .  $GLS->translate("You can add them to") . ' <a href="'. admin_url("admin.php?page=wc-settings&tab=shipping") . '" target="_blank">' . $GLS->translate('shipping zones' ) . '</a> ' . $GLS->translate("Don't forget to set the appropriate cost for each carrier if you don't have free shipping for all orders" ) . '</small></p>'; 
 	    	}
		}


		$this->print_accordion($GLS->translate('Credentials'), $content, true); 
	}

	public function print_export_options()
	{
		global $GLS; 

    	$statuses = wc_get_order_statuses();   

		$content = '<div class="gls-input-title">' . $GLS->translate('Export Preset Orders') . '</div>'; 
		$content .= '<p>' . $GLS->translate('When you click "Export Preset Orders" in the orders view, export all orders not exported successfully, with status') . ' </p>'; 
		
		foreach ( $statuses as $status_key => $status_label) {
         	$checked = get_option('GLS_export_statuses-'.$status_key) ? 'checked' : ''; 
        	$content .=  '<p><input name="GLS_export_statuses-' . $status_key . '" ' . $checked . ' type="checkbox" value="export"/>' . $status_label . '</p>'; 
        }

        $content .= '<div class="gls-input-title">' . $GLS->translate('automaticexport') . '</div>'; 
        $content .= '<p>' . $GLS->translate('automaticexportdescription') . '</p>'; 

        $content .= '<select name="GLS_autoexport"><option value="">-</option>'; 
        
        foreach ( $statuses as $status_key => $status_label) {
        	$selected = $this->autoexport_status  == $status_key ? 'selected' :''; 
        	$content .= '<option value="' . $status_key . '" ' . $selected . '>' . $status_label . '</option>';
        }

        $content .= '</select>';
 
		$this->print_accordion($GLS->translate('exportsectiontitle'), $content, false);
	}

	public function print_pickup_points_options()
	{
		global $GLS; 

		$content = $this->getTextInput($GLS->translate('googlemapskey'), 'GLS_maps_key', $this->maps_key); 
		$content .= '<p><small>' . sprintf($GLS->translate('If a google key is provided the map served will be a google map else an openmap will be shown'), "https://developers.google.com/maps/documentation/geolocation/get-api-key") . '</small></p>';

		$this->print_accordion($GLS->translate('pickuppointsoptions'), $content, false);
	}

	public function print_export_virtual() {
		global $GLS; 

		$content = '<p><input type="checkbox" name="GLS_export_virtual_products" value="1"' .  ( get_option('GLS_export_virtual_products') ? 'checked': '' ) . '/>' . $GLS->translate('exportvirtualproducts') . '<br/><input type="checkbox" name="GLS_export_virtual_orders" value="1" ' . ( get_option('GLS_export_virtual_orders') ? 'checked': '' ) . '/>' . $GLS->translate('exportvirtualorders') . '</p>'; 

	  $this->print_accordion($GLS->translate('exportvirtualtitle'), $content,false); 
 
	}

	public function print_label_options()
	{
		global $GLS; 
		$labelagree = get_option('GLS_labelagree');

		$content = '<div class="gls-input-title">' . $GLS->translate('labelprintdescription') . '</div>'; 
		$content .= '<input type="checkbox" name="GLS_labelagree" value="1"' . ($labelagree ? 'checked': '') . '/>'  . $GLS->translate('labelagree');
		$this->print_accordion($GLS->translate('labelprintingtitle'), $content, !$labelagree);
	}

	public function print_help_tab()
	{
		print '<div class="gls-tab tab">';  
		$this->print_help_export();
		$this->print_help_status(); 
		$this->print_help_labels();
		$this->print_help_labels_bulk();
		$this->print_help_wpapi(); 
		print '</div>';
	}

	public function print_help_export()
	{
		global $GLS; 

		$content = $GLS->translate('exportdescription');

		$this->print_accordion($GLS->translate('helpexporttitle'), $content, true);
	}

	public function print_help_status()
	{
		global $GLS;

		$content = '<p>' . $GLS->translate('statusdescription') . '</p>'; 
		$content .= '<span class="gls-status-list"><label class="gls-label">' . $GLS->translate('order') . '</label><ul>';
        $content .= '<li><span class="GLS-icon GLS-icon-not-exported"></span>' . $GLS->translate('notexporteddescription') . '</li>'; 
        $content .= '<li><span class="GLS-icon GLS-icon-success"></span>' . $GLS->translate('successdescription') . '</li>'; 
        $content .= '<li><span class="GLS-icon GLS-icon-error"></span>' . $GLS->translate('exporterrordescription') . '</li>';
        $content .= '</ul></span>'; 

        $content .= '<span class="gls-status-list"><label class="gls-label">' . $GLS->translate('label') . '</label><ul>';
        $content .= '<li><span class="GLS-icon GLS-icon-print-printed"></span>' . $GLS->translate('printsuccesseddescription') . '</li>'; 
        $content .= '<li><span class="GLS-icon GLS-icon-print-error"></span>' . $GLS->translate('printerrordescription') . '</li>'; 
        $content .='</ul></span>'; 

		$this->print_accordion($GLS->translate('helpstatustitle'), $content, true);
	}


	public function print_help_labels()
	{
		global $GLS;

		$content = '<ul>';
        $content .= '<li>' .  $GLS->translate('labeltermsintro') . '</li>'; 
        $content .= '<li>' .  $GLS->translate('labelbuttondescription') . '</li>'; 
        $content .= '<li><img src="' . GLS_PLUGIN_URL .'/assets/images/print-label.png"/></li>';
        $content .= '<li>' . $GLS->translate('labelterms') . '</li>';
      	$content .= '</ul>'; 

		$this->print_accordion($GLS->translate('helplabelstitle'), $content, true);
	}

	public function print_help_labels_bulk()
	{
		global $GLS;
 
		$content  = '<div class="GLS-settings__section">'; 
        $content .= $GLS->translate('labelbulkprint');
        $content .= '</div>'; 

		$this->print_accordion($GLS->translate('labelbulkprintitle'), $content, true);
	}



	public function print_hide_not_free() {
		global $GLS;

		$content  = '<div class="GLS-settings__section">'; 
		$content .= '<input type="checkbox" name="GLS_hide_not_free" value="1" ' . ( $this->hidenotfree ? 'checked': '') . '/> ' .  $GLS->translate('hidenotfree');
		$content .= '</div>';

		$this->print_accordion($GLS->translate('hidenotfreeheadtitle'), $content, $this->hidenotfree); 
	}

	public function print_help_wpapi ()
	{
		global $GLS;

		$content  = '<div class="GLS-settings__section">'; 
		
        $activateapihtml = '<p>' . $GLS->translate('usewpapi') . ':'; 
        $activateapihtml .= '<select name="GLS_usewpapi">' 
        . '<option Value="0" ' . ($this->usewpapi ? '' : 'selected') . '>' . $GLS->translate('no') .  '</option>'
        . '<option value="1" ' . ($this->usewpapi ? 'selected' : '') . '>' . $GLS->translate('yes') .  '</option>';
        $activateapihtml .= '</select></p>';

        if($this->is_api_active ) {  
          $content .= sprintf($GLS->translate('useapihelp'), $activateapihtml);  
        } 
        else { 
          $content .= sprintf($GLS->translate('useapihelpinactive'), $activateapihtml); 
        }

		$content .= '</div>';

		$this->print_accordion($GLS->translate('useapititle'), $content,false); 
	}

	public function print_flexible_checkout_field( $field, $value ) {
   $addressFields = ['CompanyName','Name','Streetname1','Streetname2','HouseNumber','NumberExtension','PostalCode','City','State','Country','Phone','Email']; 
  	$content = "<tr><td><label>" . $field['label'] . '</label></td>
  	<td><select name="GLS_co_fields_' . $field['name']  . '">
        <option value="">-</option>';
		foreach ( $addressFields as $field ) { 
		  $content .= '<option ' . ( $field == $value  ?  "selected " : '') .' value="' . $field . '">' .
		  $field .  '</option>';
		}
    $content .= '</select></td></tr>'; 
    return $content;
	}

  public function print_flexible_checkout_fields() {
    $custom_co_settings = get_option('inspire_checkout_fields_settings');
    $oursettings = get_option('GLS_custom_checkout_fields'); 
    $content = ''; 

    if( isset( $custom_co_settings['billing'] ) ) { 
      $content .= "<h4># Billing</h4><table>";
      foreach ( $custom_co_settings['billing'] as $field ) 
      {
        if ( isset( $field['custom_field'] ) ) {
          $value = isset($oursettings[$field['name']])  ? $oursettings[$field['name']] : '';
          $content .= $this->print_flexible_checkout_field( $field, $value ); 
        }
      }
      $content .= "</table>";
    }
    
    if( isset( $custom_co_settings['shipping'] ) ) {
      $content .= "<h4># Shipping</h4><table>";
      foreach ( $custom_co_settings['shipping'] as $field ) 
      {
        if ( isset( $field['custom_field'] ) ) {
          $value = isset($oursettings[$field['name']])  ? $oursettings[$field['name']] : '';
          $content .= $this->print_flexible_checkout_field( $field, $value ); 
        }
      }

      $content .= "</table>";
    }

    if ($content) {
			$this->print_accordion('Flexible checkout Fields', $content,false); 
		}
  }
}

$GLS_options_ui = new GLSOptionsUIDefault(); 