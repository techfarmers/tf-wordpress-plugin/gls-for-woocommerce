<?php 
/**  
 * print a single field in the form 
 * try to get the value from the vendor object 
 */ 
function GLS_account_print_field ( $label, $name , $vendor ) { 
?>
    <div class='form-item'>
        <label><?php echo $label ?></label>
        <input type='text' name='<?php echo $name?>' value="<?php echo isset($vendor->$name) ? $vendor->$name : ''?>"/>
    </div>
<?php     
}

/** 
 *  Object Vendor containing all fields we can pre-fill from market place info 
 */ 
function GLS_account_print_form($vendor, $error, $message) {
    $GLS = WooGLS::instance(); 
    get_header();  

    echo "<h2> " . $GLS->translate("requestaccount") . " </h2> "; 

    if($message){
        echo "$message<br/>"; 
    }

    if($message && !$error){
        die(); 
    }

    echo "<form class='GLS-request-account' action='" . get_site_url() . "/GLS-request-account?vendor_id=" . $vendor->userid . "' method='POST'>"; 
     
    GLS_account_print_field( $GLS->translate('companyname'), 'companyname',$vendor ); 
    GLS_account_print_field( $GLS->translate('contactperson'),'name', $vendor );  
    GLS_account_print_field( $GLS->translate('contactphone'),'phone',$vendor ); 
    GLS_account_print_field( $GLS->translate('fiscal'),'fiscal',$vendor ); 
    GLS_account_print_field("Email", 'email', $vendor ); 

    echo "<br/><br/><h2>Shipping information</h2>"; 
    GLS_account_print_field($GLS->translate('streetname'),'streetname', $vendor );

    GLS_account_print_field( $GLS->translate("zipcode"),'zipcode', $vendor );
    GLS_account_print_field( $GLS->translate('city'), 'city',$vendor ); 
    GLS_account_print_field( $GLS->translate('province'), 'province',$vendor ); 
    GLS_account_print_field( $GLS->translate('country'), 'country',$vendor );
    echo "<br/><br/><button type='submit'>" . $GLS->translate('submitrequest') . "</button>"; 
    echo "</form>";
}