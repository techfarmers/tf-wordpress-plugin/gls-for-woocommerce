<?php 
return array(
	'Credentials' => 'GLS-Berechtigungsnachweise',
	'new_account' => "Wenn Sie noch kein Konto haben",
	'exportsectiontitle' => 'Nach GLS exportieren', 
	'googlemapskey' => 'Google Maps Schlüssel',
	'labelprintingtitle' => 'Etikettendruck', 
	'labelprintdescription' => 'Aktivieren des Etikettendrucks in Woocommerce',
	'helpexporttitle' => 'Exportaufträge',
	'helpstatustitle' => 'Bedeutung der GLS-Status-/Aktionssymbole',
	'helplabelstitle' => 'Versandetiketten in Woocommerce erstellen',
	'hidenotfreeheadtitle' => 'Versandmethoden ausblenden'
);