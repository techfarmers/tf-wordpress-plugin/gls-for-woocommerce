<?php 
return array(
	'Credentials' => 'GLS Credentials',
	'new_account' => "If you don't have an account",
	'exportsectiontitle' => 'Export to GLS', 
	'googlemapskey' => 'Google Maps Key',
	'labelprintingtitle' => 'Label Printing', 
	'labelprintdescription' => 'Enable Label Printing in Woocommerce',
	'helpexporttitle' => 'Export Orders',
	'helpstatustitle' => 'Status order to label',
	'helplabelstitle' => 'Creating shipping labels in Woocommerce',
	'hidenotfreeheadtitle' => 'Hide Shipping Methods'
);