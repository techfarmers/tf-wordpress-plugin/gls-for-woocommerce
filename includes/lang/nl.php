<?php  
return array(
	'Credentials' => 'GLS API Sleutels',
	'new_account' => 'Als u geen GLS Shipping account heeft',
	'exportsectiontitle' => 'Exporteer naar GLS', 
	'googlemapskey' => 'Google API Sleutels',
	'labelprintingtitle' => 'Verzendlabels aanmaken', 
	'labelprintdescription' => 'Verzendlabels aanmaken in WooCommerce',
	'helpexporttitle' => 'Exporteer Bestellingen',
	'helpstatustitle' => 'Betekenis GLS Status / Actie iconen',
	'helplabelstitle' => 'Verzendlabels één voor één aanmaken in WooCommerce ',
	'hidenotfreeheadtitle' => 'Verberg verzendmethodes'
);